import os
import csv
import argparse

ap = argparse.ArgumentParser()

ap.add_argument('-f', '--fingerprints', default='/home/digitais', help='Path to the fingerprints root folder')
args = vars(ap.parse_args())    
path = args['fingerprints']

with open('paths.csv', 'w') as csv_file:
    writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    for directory in os.listdir(path):
        writer.writerow([os.path.join(path, directory)])