import sys
sys.path.insert(0,'/home/decode_nfiq/build')
from finger_id import *

import math
import numpy as np
import os
import csv
import random
import json
import argparse

import multiprocessing as mp

from PIL import Image

ap = argparse.ArgumentParser()

ap.add_argument('-f', '--fingerprints', default='/home/digitais', help='Path to the fingerprints root folder')
ap.add_argument('-p', '--path', default='paths.csv',help='Path to csv file with the fingerprints listed')
args = vars(ap.parse_args())

def np_conv2d(a, f):
	s = f.shape + tuple(np.subtract(a.shape, f.shape) + 1)
	strd = np.lib.stride_tricks.as_strided
	subM = strd(a, shape = s, strides = a.strides * 2)
	return np.einsum('ij,ijkl->kl', f, subM)

def estimate_noise_variance(img):
    H, W = img.shape
    M = np.asarray( [[1, -2, 1], [-2, 4, -2], [1, -2, 1]] )
    sigma = np.sum(np.sum(np.absolute( np_conv2d(img,M) ) ))
    sigma = sigma * math.sqrt(0.5 * math.pi) / (6 * (W-2) * (H-2))
    return sigma

def flatten(t):
    flattened_list = []
    for i in t:
        if hasattr(i, '__iter__') and not isinstance(i, str):
            for j in i:
                flattened_list.append(j)
        else:
            flattened_list.append(i)
    return flattened_list

def generate_output(id_path):
    global count
    count+=1
    print(count)
    for root, dirs, files in os.walk(id_path, topdown=False):
       # print(id_path)
        #output_dir = root.replace('digitais', 'digitais_output')
        #if not os.path.isdir(output_dir):
        #    os.makedirs(output_dir)

        for image in files:
            line = []
            line.append(os.path.join(root, image))
            image = os.path.join(root, image)
            img = read_wsq_file(image)
            try:
                noise = estimate_noise_variance(img)
            except AttributeError as e:
                print(str(e))
           # print(noise)
            line.append(noise)
            nfiq1_qualities = [nfiq1_quality(img)]
            nfiq2_qualities = [nfiq2_quality(img)]
            for enhance_filter in filters:
                enhanced_image = enhance_function(img, filter=enhance_filter)
                nfiq1_q = nfiq1_quality(enhanced_image)
                nfiq1_qualities.append(nfiq1_q)
                nfiq2_q = nfiq2_quality(enhanced_image)
                nfiq2_qualities.append(nfiq2_q)
                image_name, ext = image.split('.')
               # enhanced_image_name = f'{image_name}_{enhance_filter.lower()}.{ext}'
               # enhanced_image_path = os.path.join(output_dir,enhanced_image_name)
               # with open(enhanced_image_path,'w') as image_file:
               #     image_file.write('sda')

            line.append(nfiq1_qualities)
            line.append(nfiq2_qualities)
            
            line = flatten(line)

            with open('results.csv', 'a', newline='') as csv_file:
                writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                writer.writerow(line)

if __name__ == "__main__":
    path=r'{}'.format(args['fingerprints'])
    output_path=f"{path}_output"#path.replace('digitais','digitais_output')

    #id_list = [item for item in os.listdir(path) if os.path.isdir(os.path.join(path, item))]
    id_list = []
    with open(args['path'], 'r') as paths_file:
        for line in paths_file.readlines():
            id_list.append(line.rstrip())
    id_list = id_list
    n=len(id_list)
    filters = ['CLAHE', 'CARTOON', 'NORMALIZATION', 'LCLAHE', 'WIENER']

    with open('results.csv', 'w', newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['Path, Noise, nfiq1_Original, nfiq1_Realce1, nfiq1_Realce2, nfiq1_Realce3, nfiq1_Realce4, nfiq1_Realce5, nfiq2_Original, nfiq2_Realce1, nfiq2_Realce2, nfiq2_Realce3, nfiq2_Realce4, nfiq2_Realce5'])

    count=0

    pool = mp.Pool(14)

    results = pool.map(generate_output, [id_path for id_path in id_list])

    pool.close()
    pool.join()
